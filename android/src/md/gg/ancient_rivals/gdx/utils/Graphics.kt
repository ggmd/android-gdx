package md.gg.ancient_rivals.gdx.utils

import android.graphics.*
import android.os.Environment
import java.io.File
import java.io.FileOutputStream

fun Path.moveTo(x: Number, y: Number) = moveTo(x.toFloat(), y.toFloat())
fun Path.lineTo(x: Number, y: Number) = lineTo(x.toFloat(), y.toFloat())

fun String.createBitmap(size: Float, textColor: Int): Bitmap {
    val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textSize = size
        color = textColor
        textAlign = Paint.Align.LEFT
    }

    val baseline = -paint.ascent()
    val w = paint.measureText(this) + 0.5
    val h = baseline + paint.descent() + 0.5

    val image = Bitmap.createBitmap(w.toInt(), h.toInt(), Bitmap.Config.ARGB_8888)
    val canvas = Canvas(image)
    canvas.drawText(this, 0F, baseline, paint)
    return image
}

fun Bitmap.save(name: String) {
    val root = Environment.getExternalStorageDirectory().toString()
    val myDir = File(root + "/ancient_rivals_bitmaps")
    myDir.mkdirs()

    val fileName = "$name.png"
    val file = File(myDir, fileName)
    if (file.exists()) {
        file.delete()
    }

    val out = FileOutputStream(file)
    compress(Bitmap.CompressFormat.PNG, 90, out)
    out.flush()
    out.close()
}

/**
 * WARNING: may throw null pointer exception
 * */
fun Bitmap.repeat(x: Double, y: Double): Bitmap {
    val bitmap = Bitmap.createBitmap((x * width).toInt(), (y * width).toInt(), config)
    val canvas = Canvas(bitmap)
    val srcRect = Rect(0, 0, width, height)
    val paint = Paint()
    for (i in 0..Math.ceil(x).toInt() - 1) {
        for (j in 0..Math.ceil(y).toInt() - 1) {
            val destRect = Rect(i * width, j * height, i * width + width, j * height + height)
            canvas.drawBitmap(this, srcRect, destRect, paint)
        }
    }
    return bitmap
}