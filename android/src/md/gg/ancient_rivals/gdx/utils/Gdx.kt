package md.gg.ancient_rivals.gdx.utils

import android.graphics.Bitmap
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.math.Vector2
import java.io.ByteArrayOutputStream

fun v2Distance(v1: Vector2, v2: Vector2): Float {
    val x = Math.abs(v1.x - v2.x)
    val y = Math.abs(v1.y - v2.y)
    return Math.sqrt((x * x + y * y).toDouble()).toFloat()
}

fun Bitmap.gdx(): Pixmap {
    val stream = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.PNG, 100, stream)
    val bytes = stream.toByteArray()

    return Pixmap(bytes, 0, bytes.size)
}
