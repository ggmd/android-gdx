package md.gg.ancient_rivals.gdx.app

import android.app.Application
import md.gg.ancient_rivals.gdx.app.di.AppComponent
import md.gg.ancient_rivals.gdx.app.di.AppModule
import md.gg.ancient_rivals.gdx.app.di.DaggerAppComponent
import timber.log.Timber

class AncientRivalsApp : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}