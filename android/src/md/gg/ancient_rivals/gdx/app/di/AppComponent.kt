package md.gg.ancient_rivals.gdx.app.di

import dagger.Component
import md.gg.ancient_rivals.gdx.data.MapRepository

@AppScope
@Component(modules = arrayOf(AppModule::class, DataModule::class))
interface AppComponent {
    fun mapRepository(): MapRepository
}
