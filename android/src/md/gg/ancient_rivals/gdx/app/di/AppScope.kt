package md.gg.ancient_rivals.gdx.app.di

import javax.inject.Scope

@Scope @Retention annotation class AppScope
