package md.gg.ancient_rivals.gdx.app.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import md.gg.ancient_rivals.gdx.data.MapRepository

@Module
class DataModule {
    @AppScope
    @Provides
    fun gson(): Gson {
        return GsonBuilder().create()
    }

    @AppScope
    @Provides
    fun mapRepository(context: Context, gson: Gson): MapRepository {
        return MapRepository(context, gson)
    }
}
