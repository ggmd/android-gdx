package md.gg.ancient_rivals.gdx.app.di

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class AppModule(val context: Context) {
    @AppScope
    @Provides
    fun context(): Context {
        return context
    }
}
