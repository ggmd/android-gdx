package md.gg.ancient_rivals.gdx.water;

import com.badlogic.gdx.math.Vector2;

import java.util.Random;

class WaveTrain {

    private static final int RISING = 1;
    private static final int FLOWING = 2;
    private static final int FALLING = 3;

    private static final int TRANSITION_TIME = 3;

    private static final int MEDIAN_WAVE_LENGTH = 32;
    private static final float W_AMPLITUDE_RATIO = 0.64f;
    private static final int GRAVITY = 16;
    private static final int WIND_ANGLE = 32;
    private static final double SPEED = 8;

    private Random random;

    private Vector2 direction;
    private double a;

    private double w;
    private double fy;

    private float life;
    private float time;
    private float stateFactor;
    private int state;

    WaveTrain() {
        random = new Random();
        direction = new Vector2();
        initialize();
    }

    private void initialize() {
        time = 0;
        stateFactor = 0;
        state = RISING;

        double waveLength = MEDIAN_WAVE_LENGTH / 2 + random.nextInt(2 * MEDIAN_WAVE_LENGTH);
        w = Math.sqrt(GRAVITY * 2 * Math.PI / waveLength);

        a = W_AMPLITUDE_RATIO / w;
        fy = SPEED * 2 * Math.PI / waveLength;
        double angle = WIND_ANGLE - 20 + random.nextInt(40);
        direction.set((float) Math.cos(angle * Math.PI / 180), (float) Math.sin(angle * Math.PI / 180));

        life = 5 + random.nextInt(10);
    }

    double w() {
        return w;
    }

    double fy() {
        return fy;
    }

    public double a() {
        return a * stateFactor / TRANSITION_TIME;
    }

    Vector2 getDirection() {
        return direction;
    }

    void update(float deltaTime) {

        switch (state) {
            case RISING:
                stateFactor += deltaTime;
                if (stateFactor > TRANSITION_TIME) {
                    stateFactor = 3;
                    state = FLOWING;
                }
                break;

            case FLOWING:
                time += deltaTime;
                if (time > life) {
                    state = FALLING;
                }
                break;

            case FALLING:
                stateFactor -= deltaTime;
                if (stateFactor < 0) {
                    initialize();
                }
                break;
        }
    }

}