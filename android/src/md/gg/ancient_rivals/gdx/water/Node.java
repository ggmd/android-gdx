package md.gg.ancient_rivals.gdx.water;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

class Node {

    private Vector3 position;
    private Vector3 normal;
    private Vector2 initialPosition;

    Node(Vector3 postion) {
        this.position = postion;
        this.normal = new Vector3();
        initialPosition = new Vector2(postion.x, postion.y);
    }

    Vector3 getPosition() {
        return position;
    }

    Vector3 getNormal() {
        return normal;
    }

    void addToNormal(Vector3 normal) {
        this.normal.add(normal);
    }

    Vector2 getInitialPosition() {
        return initialPosition;
    }

    void normalizeNormal() {
        this.normal = this.normal.nor();
    }
}