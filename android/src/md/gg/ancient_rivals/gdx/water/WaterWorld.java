package md.gg.ancient_rivals.gdx.water;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

import md.gg.ancient_rivals.gdx.main.MainSceneManager;

import static md.gg.ancient_rivals.gdx.main.Map3DGeneratorKt.MAP_HEIGHT;
import static md.gg.ancient_rivals.gdx.main.Map3DGeneratorKt.SEA_LEVEL;

public class WaterWorld {

    public static final int TRAIN_COUNT = 32;
    private static final int STATE_ONE_WAVE = 1;
    private static final int STATE_MULTIPLE_WAVES = 2;
    private static final int STATE_SHADED = 3;
    private static final int STATE_ENIVRONMENT = 4;
    private static final int NODE_DENSITY = 64;
    public int trainCount;
    private Node[][] nodes;
    private ArrayList<WaveTrain> waveTrains;
    private Model model;
    private ModelInstance modelInstance;
    private ModelBuilder modelBuilder;
    private Shader shader;
    private int state;
    private float time = 0;
    private Vector2 positionFactor = new Vector2();
    private ArrayList<Float> vertexList = new ArrayList<>();
    private float[] vertexArray = new float[(3 + 3) * 4 * (NODE_DENSITY - 1) * (NODE_DENSITY - 1)];
    private MeshPartBuilder.VertexInfo v1 = new MeshPartBuilder.VertexInfo();
    private MeshPartBuilder.VertexInfo v2 = new MeshPartBuilder.VertexInfo();
    private MeshPartBuilder.VertexInfo v3 = new MeshPartBuilder.VertexInfo();
    private MeshPartBuilder.VertexInfo v4 = new MeshPartBuilder.VertexInfo();
    private Vector3 u = new Vector3();
    private Vector3 v = new Vector3();

    public WaterWorld(AssetManager assetManager, float xOff, float zOff, float posX, float posZ) {
        nodes = new Node[NODE_DENSITY][NODE_DENSITY];

        float xStep = xOff * 2f / NODE_DENSITY;
        float yStep = zOff * 2f / NODE_DENSITY;

        for (int i = 0; i < nodes.length; i++) {
            for (int j = 0; j < nodes[0].length; j++) {
                nodes[i][j] = new Node(new Vector3(j * xStep - xOff + posX, i * yStep - zOff + posZ, 0));
            }
        }

        updateNodeNormals();

        waveTrains = new ArrayList<>();

        modelBuilder = new ModelBuilder();
        modelBuilder.begin();
        createMesh();
        model = modelBuilder.end();

        modelInstance = new ModelInstance(model);
        new EnvironmentCubemap(assetManager.get(MainSceneManager.SKYBOX_PATH, Pixmap.class));

        shader = new TestShader();
        shader.init();

        state = STATE_SHADED;
    }

    private void resetNodePositions() {
        for (Node[] node : nodes) {
            for (int j = 0; j < nodes[0].length; j++) {
                node[j].getPosition().x = node[j].getInitialPosition().x;
                node[j].getPosition().y = node[j].getInitialPosition().y;
                node[j].getPosition().z = 0;
            }
        }
    }

    public void initializeWaveTrains() {
        for (int i = 0; i < trainCount; i++) {
            waveTrains.add(new WaveTrain());
        }
    }

    private void updateNodePositions(float deltaTime) {
        resetNodePositions();
        for (WaveTrain waveTrain : waveTrains) {
            for (Node[] node : nodes) {
                for (int j = 0; j < nodes[0].length; j++) {
                    float scalingFactor = (float) (waveTrain.a() * Math.cos(waveTrain.fy() * time + waveTrain.w() * waveTrain.getDirection().dot(node[j].getInitialPosition())));
                    positionFactor.set(waveTrain.getDirection());
                    positionFactor.nor().scl(scalingFactor);

                    node[j].getPosition().x += positionFactor.x;
                    node[j].getPosition().y += positionFactor.y;
                    node[j].getPosition().z += (float) (waveTrain.a() * Math.sin(waveTrain.fy() * time + waveTrain.w() * waveTrain.getDirection().dot(node[j].getInitialPosition())));
                }
            }

            waveTrain.update(deltaTime);
        }

        time += deltaTime;
    }

    private void resetNodeNormals() {
        for (Node[] node : nodes) {
            for (int j = 0; j < nodes[0].length; j++) {
                node[j].getNormal().x = 0;
                node[j].getNormal().y = 0;
                node[j].getNormal().z = 0;
            }
        }
    }

    private void updateNodeNormals() {
        resetNodeNormals();
        for (int i = 0; i < nodes.length - 1; i++) {
            for (int j = 0; j < nodes[0].length - 1; j++) {
                addToNormals(nodes[i][j], nodes[i][j + 1], nodes[i + 1][j]);
                addToNormals(nodes[i][j + 1], nodes[i + 1][j + 1], nodes[i + 1][j]);
            }
        }

        for (Node[] node : nodes) {
            for (int j = 0; j < nodes[0].length; j++) {
                node[j].normalizeNormal();
            }
        }
    }

    private void createMesh() {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                modelBuilder.node().id = "part" + i + j;
                MeshPartBuilder meshBuilder = modelBuilder.part("part" + i + j, GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material());

                for (int row = i * NODE_DENSITY / 2; row < (i + 1) * NODE_DENSITY / 2; row++) {
                    for (int column = j * NODE_DENSITY / 2; column < (j + 1) * NODE_DENSITY / 2; column++) {
                        if (row == NODE_DENSITY - 1 || column == NODE_DENSITY - 1) {
                            continue;
                        }

                        v1.setPos(nodes[row][column].getPosition());
                        v1.setNor(nodes[row][column].getNormal());
                        short index1 = meshBuilder.vertex(v1);

                        v2.setPos(nodes[row][column + 1].getPosition());
                        v2.setNor(nodes[row][column + 1].getNormal());
                        short index2 = meshBuilder.vertex(v2);

                        v3.setPos(nodes[row + 1][column].getPosition());
                        v3.setNor(nodes[row + 1][column].getNormal());
                        short index3 = meshBuilder.vertex(v3);

                        v4.setPos(nodes[row + 1][column + 1].getPosition());
                        v4.setNor(nodes[row + 1][column + 1].getNormal());
                        short index4 = meshBuilder.vertex(v4);

                        meshBuilder.triangle(index1, index2, index3);
                        meshBuilder.triangle(index2, index4, index3);
                    }
                }

            }
        }
    }

    private void addToNormals(Node node1, Node node2, Node node3) {
        u.set(node1.getPosition());
        v.set(node2.getPosition());

        v.sub(node3.getPosition());

        u.crs(v);

        node1.addToNormal(u);
        node2.addToNormal(u);
        node3.addToNormal(u);
    }

    private void updateMesh() {
        vertexList.clear();

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {

                for (int row = i * NODE_DENSITY / 2; row < (i + 1) * NODE_DENSITY / 2; row++) {
                    for (int column = j * NODE_DENSITY / 2; column < (j + 1) * NODE_DENSITY / 2; column++) {
                        if (row == NODE_DENSITY - 1 || column == NODE_DENSITY - 1) {
                            continue;
                        }

                        setVertices(vertexList, nodes[row][column]);
                        setVertices(vertexList, nodes[row][column + 1]);
                        setVertices(vertexList, nodes[row + 1][column]);
                        setVertices(vertexList, nodes[row + 1][column + 1]);
                    }
                }

                int index = 0;
                for (Float f : vertexList) {
                    vertexArray[index++] = f;
                }

                Mesh mesh = model.getNode("part" + i + j).parts.get(0).meshPart.mesh;
                mesh.updateVertices(0, vertexArray);
            }
        }
    }

    private void setVertices(ArrayList<Float> vertexList, Node node) {
        vertexList.add(node.getPosition().x);
        vertexList.add(node.getPosition().y);
        vertexList.add(node.getPosition().z);

        vertexList.add(node.getNormal().x);
        vertexList.add(node.getNormal().y);
        vertexList.add(node.getNormal().z);
    }

    public void update(float deltaTime) {
        switch (state) {
            case STATE_ONE_WAVE:
            case STATE_MULTIPLE_WAVES:
            case STATE_SHADED:
            case STATE_ENIVRONMENT:
                updateNodePositions(deltaTime);
                updateNodeNormals();
                updateMesh();
                break;
        }
    }

    public void render(ModelBatch modelBatch) {
        modelInstance.transform.rotate(1f, 0f, 0f, -90f);
        modelInstance.transform.translate(0f, 0f, (float) SEA_LEVEL * MAP_HEIGHT * 0.72f);
        modelBatch.render(modelInstance, shader);
        modelInstance.transform.translate(0f, 0f, (float) -SEA_LEVEL * MAP_HEIGHT * 0.72f);
        modelInstance.transform.rotate(1f, 0f, 0f, 90f);
    }
}