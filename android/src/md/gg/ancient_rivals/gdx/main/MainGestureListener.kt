package md.gg.ancient_rivals.gdx.main

import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector2
import io.reactivex.subjects.BehaviorSubject
import md.gg.ancient_rivals.gdx.utils.v2Distance


class MainGestureListener : GestureDetector.GestureListener {
    val onPan: BehaviorSubject<Pair<Float, Float>> = BehaviorSubject.create()
    val onZoom: BehaviorSubject<Float> = BehaviorSubject.create()

    override fun fling(velocityX: Float, velocityY: Float, button: Int): Boolean {
        return false
    }

    override fun zoom(initialDistance: Float, distance: Float): Boolean {
        return false
    }

    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean {
        onPan.onNext(Pair(deltaX, deltaY))
        return true
    }

    override fun pinchStop() {

    }

    override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
        return false
    }

    override fun panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun longPress(x: Float, y: Float): Boolean {
        return false
    }

    override fun touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun pinch(initialPointer1: Vector2?, initialPointer2: Vector2?, pointer1: Vector2?, pointer2: Vector2?): Boolean {
        val initialDistance = v2Distance(initialPointer1 as Vector2, initialPointer2 as Vector2)
        val newDistance = v2Distance(pointer1 as Vector2, pointer2 as Vector2)
        onZoom.onNext(initialDistance - newDistance)
        return true
    }
}