package md.gg.ancient_rivals.gdx.main

import android.content.Context
import android.graphics.*
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.github.czyzby.noise4j.map.Grid
import com.github.czyzby.noise4j.map.generator.noise.NoiseGenerator
import md.gg.ancient_rivals.gdx.data.domain.*
import md.gg.ancient_rivals.gdx.main.MainSceneManager.Companion.GROUND_PATH
import md.gg.ancient_rivals.gdx.utils.gdx

const val SEA_LEVEL = 0.16
const val MAP_HEIGHT = 64f

class Map3DGenerator(private val context: Context) {
    private var map: GameMap? = null
    private var mapWidth: Int = 0
    private var mapHeight: Int = 0

    fun generate(map: GameMap, assetManager: AssetManager): List<Model> {
        this.map = map
        mapWidth = map.offset.first.times(2).toInt()
        mapHeight = map.offset.second.times(2).toInt()

        val bitmap = createHeightBitmap()

        val terrainMeshes = createSimpleGround(bitmap)
        bitmap.recycle()

        val groundMesh = terrainMeshes.first()
        val groundTexture: Texture = assetManager.get(GROUND_PATH)
        groundTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)

        val mapMesh = terrainMeshes[1]
        val mapTexture = createMapTexture()

        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        modelBuilder.part("groundPart", groundMesh, GL20.GL_TRIANGLES,
                Material(TextureAttribute.createDiffuse(groundTexture)))
        val groundModel = modelBuilder.end()

        modelBuilder.begin()
        modelBuilder.part("mapPart", mapMesh, GL20.GL_TRIANGLES,
                Material(TextureAttribute.createDiffuse(mapTexture), BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)))
        val mapModel = modelBuilder.end()

        val xOff: Float = mapWidth / 2f
        val zOff: Float = mapHeight / 2f
        val yOff: Float = (SEA_LEVEL * MAP_HEIGHT).toFloat()

        val seaTexture = createWaterTexture()
        val waterModel = modelBuilder.createRect(
                -xOff, yOff, zOff,
                xOff, yOff, zOff,
                xOff, yOff, -zOff,
                -xOff, yOff, -zOff,
                0f, 1f, 0f,
                Material(TextureAttribute.createDiffuse(seaTexture), BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)),
                (VertexAttributes.Usage.Position or VertexAttributes.Usage.Normal or VertexAttributes.Usage.TextureCoordinates).toLong()
        )

        return listOf(groundModel, mapModel, waterModel)
    }

    private fun createWaterTexture(): Texture {
        val edges = map?.seaConnections()
        val config = Bitmap.Config.ARGB_8888
        val bitmap = Bitmap.createBitmap(mapWidth, mapHeight, config)
        val canvas = Canvas(bitmap)
        val paint = Paint().apply {
            color = Color.argb(0, 0, 0, 0)
            style = Paint.Style.FILL
        }
        val fullRect = Rect(0, 0, canvas.width, canvas.height)
        canvas.drawRect(fullRect, paint)
        paint.color = Color.argb(255, 224, 224, 224)
        paint.strokeWidth = 4.0f
        if (edges != null) {
            for ((first, second) in edges) {
                canvas.drawLine(first.x.toFloat() + canvas.width / 2, first.y.toFloat() + canvas.height / 2,
                        second.x.toFloat() + canvas.width / 2, second.y.toFloat() + canvas.height / 2, paint)
            }
        }

        drawSeaNames(canvas)

        val texture = Texture(bitmap.gdx())
        bitmap.recycle()
        return texture
    }

    private fun createSimpleGround(bitmap: Bitmap): List<Mesh> {
        val scaleFactor = 4.0f
        val aspect = mapHeight.toFloat() / mapWidth

        val groundMesh = createTerrainMesh(bitmap, Pair(scaleFactor, scaleFactor * aspect))
        val mapMesh = createTerrainMesh(bitmap, Pair(1f, 1f))

        return listOf(groundMesh, mapMesh)
    }

    private fun createTerrainMesh(bitmap: Bitmap, uv: Pair<Float, Float>): Mesh {
        val scaled = Bitmap.createScaledBitmap(bitmap, 128, 128, true)

        val pixmap = scaled.gdx()

        val field = HeightField(true, pixmap, true,
                VertexAttributes.Usage.Position or
                        VertexAttributes.Usage.Normal or
                        VertexAttributes.Usage.ColorUnpacked or
                        VertexAttributes.Usage.TextureCoordinates)
        pixmap.dispose()
        scaled.recycle()

        val xOff: Float = mapWidth / 2f
        val zOff: Float = mapHeight / 2f

        field.apply {
            corner00.set(-xOff, 0.0f, -zOff)
            corner10.set(xOff, 0f, -zOff)
            corner01.set(-xOff, 0f, zOff)
            corner11.set(xOff, 0f, zOff)

            val tint = 0.96f

            color00.set(tint, tint, tint, tint)
            color01.set(tint, tint, tint, tint)
            color10.set(tint, tint, tint, tint)
            color11.set(tint, tint, tint, tint)

            magnitude.set(1f, MAP_HEIGHT, 1f)

            uvScale.set(uv.first, uv.second)

            update()
        }
        return field.mesh
    }

    private fun createMapTexture(): Texture {
        val edges = map?.terrainConnections()
        val config = Bitmap.Config.ARGB_8888
        val bitmap = Bitmap.createBitmap(mapWidth, mapHeight, config)
        val canvas = Canvas(bitmap)
        val paint = Paint().apply {
            color = Color.argb(0, 0, 0, 0)
            style = Paint.Style.FILL
        }
        val fullRect = Rect(0, 0, canvas.width, canvas.height)
        canvas.drawRect(fullRect, paint)
        paint.color = Color.argb(255, 16, 16, 16)
        paint.strokeWidth = 4.0f
        if (edges != null) {
            for ((first, second) in edges) {
                canvas.drawLine(first.x.toFloat() + canvas.width / 2, first.y.toFloat() + canvas.height / 2,
                        second.x.toFloat() + canvas.width / 2, second.y.toFloat() + canvas.height / 2, paint)
            }
        }

        drawTerrainNames(canvas)

        val texture = Texture(bitmap.gdx())
        bitmap.recycle()
        return texture
    }

    private fun drawTerrainNames(canvas: Canvas) {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            textSize = 14f
            color = Color.argb(255, 255, 255, 255)
        }
        map?.provinces?.filter {
            it.type == ProvinceType.TERRAIN
        }?.forEach {
            val position = it.placeName()
            val w = paint.measureText(it.name)
            canvas.drawText(it.name, position.first.toFloat() + canvas.width / 2 - w / 2, position.second.toFloat() + canvas.height / 2, paint)
        }
    }

    private fun drawSeaNames(canvas: Canvas) {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            textSize = 14f
            color = Color.argb(255, 0, 0, 0)
        }
        map?.provinces?.filter {
            it.type == ProvinceType.SEA
        }?.forEach {
            val position = it.placeName()
            val w = paint.measureText(it.name)
            canvas.drawText(it.name, position.first.toFloat() + canvas.width / 2 - w / 2, position.second.toFloat() + canvas.height / 2, paint)
        }
    }

    private fun createHeightBitmap(): Bitmap {
        val config = Bitmap.Config.ARGB_8888
        val bitmap = Bitmap.createBitmap(128, 128, config)

        applyNoise(bitmap)
        val targetBitmap = Bitmap.createScaledBitmap(bitmap, mapWidth, mapHeight, true)
        val canvas = Canvas(targetBitmap)
        createPlaceForSeas(canvas)

        return targetBitmap
    }

    private fun applyNoise(bitmap: Bitmap) {
        val noiseGenerator = NoiseGenerator()
        val grid = Grid(128)
        noiseStage(grid, noiseGenerator, 4, 196f)
        val pixels = IntArray(bitmap.width * bitmap.height)
        for (i in 0..bitmap.width - 1) {
            for (j in 0..bitmap.height - 1) {
                val cell = grid.get(i, j)
                var intCell = cell.toInt()
                if (intCell <= SEA_LEVEL * 255) {
                    intCell = (SEA_LEVEL * 255 + 1).toInt()
                }
                pixels[j * bitmap.width + i] = android.graphics.Color.argb(255, intCell, intCell, intCell)
            }
        }
        bitmap.setPixels(pixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
    }

    private fun noiseStage(grid: Grid, noiseGenerator: NoiseGenerator, radius: Int, modifier: Float) {
        noiseGenerator.apply {
            this.radius = radius
            this.modifier = modifier
            generate(grid)
        }
    }

    private fun createPlaceForSeas(canvas: Canvas) {
        val paint = Paint().apply {
            style = Paint.Style.FILL
        }

        paint.color = android.graphics.Color.BLACK
        map?.provinces?.filter { it.type == ProvinceType.SEA }!!
                .map { provincePath(it, Pair(mapWidth, mapHeight)) }
                .forEach { canvas.drawPath(it, paint) }
    }
}
