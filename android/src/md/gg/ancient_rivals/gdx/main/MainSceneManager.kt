package md.gg.ancient_rivals.gdx.main

import android.content.Context
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL20.*
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector3
import io.reactivex.subjects.BehaviorSubject
import md.gg.ancient_rivals.gdx.data.domain.GameMap
import md.gg.ancient_rivals.gdx.water.WaterWorld

class MainSceneManager(context: Context, private val mapGenerator: Map3DGenerator) : ApplicationAdapter() {
    val onInit: BehaviorSubject<Unit> = BehaviorSubject.create()

    private lateinit var camera: PerspectiveCamera
    private lateinit var modelBatch: ModelBatch
    private lateinit var environment: Environment

    private var groundModel: Model? = null
    private var groundInstance: ModelInstance? = null

    private var mapModel: Model? = null
    private var mapInstance: ModelInstance? = null

    private var waterMapModel: Model? = null
    private var waterMapInstance: ModelInstance? = null

    private val assetManager = AssetManager()

    private val scaleSpeed = 0.1

    private var waterWorld: WaterWorld? = null


    fun registerGestureListener(gestureListener: MainGestureListener) {
        Gdx.input.inputProcessor = GestureDetector(gestureListener)
    }

    fun pan(distance: Pair<Float, Float>) {
        camera.apply {
            position.x -= distance.first
            position.z -= distance.second
        }
    }

    fun scale(factor: Double) {
        camera.position.y += ((factor - 1.0) * scaleSpeed).toFloat()
    }

    fun buildMap(map: GameMap) {
        val models = mapGenerator.generate(map, assetManager)
        groundModel = models[0]
        mapModel = models[1]
        waterMapModel = models[2]

        groundInstance = ModelInstance(groundModel)
        mapInstance = ModelInstance(mapModel)
        mapInstance?.transform?.translate(0f, 1f, 0f)
        waterMapInstance = ModelInstance(waterMapModel)

        waterWorld = WaterWorld(
                assetManager,
                1000f, 500f, 0f, 0f)
        waterWorld?.trainCount = WaterWorld.TRAIN_COUNT
        waterWorld?.initializeWaveTrains()
    }

    override fun create() {
        Gdx.gl.glEnable(GL_BLEND)
        Gdx.gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        camera = PerspectiveCamera(67F, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat()).apply {
            position.set(0f, 1f, 1f)
            lookAt(0f, 1f, 0f)
            position.set(0f, 500f, 500f)
            rotate(Vector3.X, -75f)
            far = 2000f
            near = 1f
            update()
        }

        modelBatch = ModelBatch()

        environment = Environment().apply {
            set(ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f))
            add(DirectionalLight().set(0.8f, 0.8f, 0.8f, -0.1f, -0.8f, -0.2f))
        }

        assetManager.load(GROUND_PATH, Texture::class.java)
        assetManager.load(SKYBOX_PATH, Pixmap::class.java)
    }

    override fun render() {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        if (assetManager.update()) {
            if (!onInit.hasComplete()) {
                onInit.onNext(Unit)
                onInit.onComplete()
            }
        }

        modelBatch.begin(camera)
        if (groundInstance != null) {
            waterWorld?.render(modelBatch)
            modelBatch.render(groundInstance, environment)
            modelBatch.render(mapInstance, environment)
            modelBatch.render(waterMapInstance, environment)
        }
        modelBatch.end()

        camera.update()
        waterWorld?.update(Gdx.graphics.deltaTime)
    }

    override fun dispose() {
        modelBatch.dispose()
        assetManager.dispose()
        groundModel?.dispose()
    }

    companion object {
        const val GROUND_PATH = "ground2.png"
        const val SKYBOX_PATH = "skybox.png"
    }
}