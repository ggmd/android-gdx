package md.gg.ancient_rivals.gdx.main

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder.VertexInfo
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.GdxRuntimeException
import java.nio.ByteBuffer


class HeightField(isStatic: Boolean, val width: Int, val height: Int, smooth: Boolean, attributes: VertexAttributes) : Disposable {
    val uvOffset = Vector2(0f, 0f)
    val uvScale = Vector2(1f, 1f)
    val color00 = Color(Color.WHITE)
    val color10 = Color(Color.WHITE)
    val color01 = Color(Color.WHITE)
    val color11 = Color(Color.WHITE)
    val corner00 = Vector3(0f, 0f, 0f)
    val corner10 = Vector3(1f, 0f, 0f)
    val corner01 = Vector3(0f, 0f, 1f)
    val corner11 = Vector3(1f, 0f, 1f)
    val magnitude = Vector3(0f, 1f, 0f)

    val data: FloatArray
    val smooth: Boolean
    val mesh: Mesh

    private val vertices: FloatArray
    private val stride: Int

    private val posPos: Int
    private val norPos: Int
    private val uvPos: Int
    private val colPos: Int

    private val vertex00 = MeshPartBuilder.VertexInfo()
    private val vertex10 = MeshPartBuilder.VertexInfo()
    private val vertex01 = MeshPartBuilder.VertexInfo()
    private val vertex11 = MeshPartBuilder.VertexInfo()

    private val tmpV1 = Vector3()
    private val tmpV2 = Vector3()
    private val tmpV3 = Vector3()
    private val tmpV4 = Vector3()
    private val tmpV5 = Vector3()
    private val tmpV6 = Vector3()
    private val tmpV7 = Vector3()
    private val tmpV8 = Vector3()
    private val tmpV9 = Vector3()
    private val tmpC = Color()

    constructor(isStatic: Boolean, map: Pixmap, smooth: Boolean, attributes: Int) : this(isStatic, map.width, map.height, smooth, attributes) {
        set(map)
    }

    constructor(isStatic: Boolean, colorData: ByteBuffer, format: Pixmap.Format, width: Int, height: Int,
                smooth: Boolean, attributes: Int) : this(isStatic, width, height, smooth, attributes) {
        set(colorData, format)
    }

    constructor(isStatic: Boolean, data: FloatArray, width: Int, height: Int, smooth: Boolean, attributes: Int) : this(isStatic, width, height, smooth, attributes) {
        set(data)
    }

    constructor(isStatic: Boolean, width: Int, height: Int, smooth: Boolean, attributes: Int) : this(isStatic, width, height, smooth, MeshBuilder.createAttributes(attributes.toLong()))

    init {
        var smooth = smooth
        this.posPos = attributes.getOffset(Usage.Position, -1)
        this.norPos = attributes.getOffset(Usage.Normal, -1)
        this.uvPos = attributes.getOffset(Usage.TextureCoordinates, -1)
        this.colPos = attributes.getOffset(Usage.ColorUnpacked, -1)
        smooth = smooth || norPos < 0 // cant have sharp edges without normals
        this.smooth = smooth
        this.data = FloatArray(width * height)

        this.stride = attributes.vertexSize / 4

        val numVertices = if (smooth) width * height else (width - 1) * (height - 1) * 4
        val numIndices = (width - 1) * (height - 1) * 6

        this.mesh = Mesh(isStatic, numVertices, numIndices, attributes)
        this.vertices = FloatArray(numVertices * stride)

        setIndices()
    }

    private fun setIndices() {
        val w = width - 1
        val h = height - 1
        val indices = ShortArray(w * h * 6)
        var i = -1
        for (y in 0..h - 1) {
            for (x in 0..w - 1) {
                val c00 = if (smooth) y * width + x else y * 2 * w + x * 2
                val c10 = c00 + 1
                val c01 = c00 + if (smooth) width else w * 2
                val c11 = c10 + if (smooth) width else w * 2
                indices[++i] = c11.toShort()
                indices[++i] = c10.toShort()
                indices[++i] = c00.toShort()
                indices[++i] = c00.toShort()
                indices[++i] = c01.toShort()
                indices[++i] = c11.toShort()
            }
        }
        mesh.setIndices(indices)
    }

    fun update() {
        if (smooth) {
            if (norPos < 0)
                updateSimple()
            else
                updateSmooth()
        } else
            updateSharp()
    }

    private fun updateSmooth() {
        for (x in 0..width - 1) {
            for (y in 0..height - 1) {
                val v = getVertexAt(vertex00, x, y)
                getWeightedNormalAt(v.normal, x, y)
                setVertex(y * width + x, v)
            }
        }
        mesh.setVertices(vertices)
    }

    private fun updateSimple() {
        for (x in 0..width - 1) {
            for (y in 0..height - 1) {
                setVertex(y * width + x, getVertexAt(vertex00, x, y))
            }
        }
        mesh.setVertices(vertices)
    }

    private fun updateSharp() {
        val w = width - 1
        val h = height - 1
        for (y in 0..h - 1) {
            for (x in 0..w - 1) {
                val c00 = y * 2 * w + x * 2
                val c10 = c00 + 1
                val c01 = c00 + w * 2
                val c11 = c10 + w * 2
                val v00 = getVertexAt(vertex00, x, y)
                val v10 = getVertexAt(vertex10, x + 1, y)
                val v01 = getVertexAt(vertex01, x, y + 1)
                val v11 = getVertexAt(vertex11, x + 1, y + 1)
                v01.normal.set(v01.position).sub(v00.position).nor().crs(tmpV1.set(v11.position).sub(v01.position).nor())
                v10.normal.set(v10.position).sub(v11.position).nor().crs(tmpV1.set(v00.position).sub(v10.position).nor())
                v00.normal.set(v01.normal).lerp(v10.normal, .5f)
                v11.normal.set(v00.normal)

                setVertex(c00, v00)
                setVertex(c10, v10)
                setVertex(c01, v01)
                setVertex(c11, v11)
            }
        }
        mesh.setVertices(vertices)
    }

    /** Does not set the normal member!  */
    private fun getVertexAt(out: VertexInfo, x: Int, y: Int): VertexInfo {
        val dx = x.toFloat() / (width - 1).toFloat()
        val dy = y.toFloat() / (height - 1).toFloat()
        val a = data[y * width + x]
        out.position.set(corner00).lerp(corner10, dx).lerp(tmpV1.set(corner01).lerp(corner11, dx), dy)
        out.position.add(tmpV1.set(magnitude).scl(a))
        out.color.set(color00).lerp(color10, dx).lerp(tmpC.set(color01).lerp(color11, dx), dy)
        out.uv.set(dx, dy).scl(uvScale).add(uvOffset)
        return out
    }

    fun getPositionAt(out: Vector3, x: Int, y: Int): Vector3 {
        val dx = x.toFloat() / (width - 1).toFloat()
        val dy = y.toFloat() / (height - 1).toFloat()
        val a = data[y * width + x]
        out.set(corner00).lerp(corner10, dx).lerp(tmpV1.set(corner01).lerp(corner11, dx), dy)
        out.add(tmpV1.set(magnitude).scl(a))
        return out
    }

    fun getWeightedNormalAt(out: Vector3, x: Int, y: Int): Vector3 {
        var faces = 0
        out.set(0f, 0f, 0f)

        val center = getPositionAt(tmpV2, x, y)
        val left = if (x > 0) getPositionAt(tmpV3, x - 1, y) else null
        val right = if (x < width - 1) getPositionAt(tmpV4, x + 1, y) else null
        val bottom = if (y > 0) getPositionAt(tmpV5, x, y - 1) else null
        val top = if (y < height - 1) getPositionAt(tmpV6, x, y + 1) else null
        if (top != null && left != null) {
            out.add(tmpV7.set(top).sub(center).nor().crs(tmpV8.set(center).sub(left).nor()).nor())
            faces++
        }
        if (left != null && bottom != null) {
            out.add(tmpV7.set(left).sub(center).nor().crs(tmpV8.set(center).sub(bottom).nor()).nor())
            faces++
        }
        if (bottom != null && right != null) {
            out.add(tmpV7.set(bottom).sub(center).nor().crs(tmpV8.set(center).sub(right).nor()).nor())
            faces++
        }
        if (right != null && top != null) {
            out.add(tmpV7.set(right).sub(center).nor().crs(tmpV8.set(center).sub(top).nor()).nor())
            faces++
        }
        if (faces != 0)
            out.scl(1f / faces.toFloat())
        else
            out.set(magnitude).nor()
        return out
    }

    private fun setVertex(index: Int, info: VertexInfo) {
        var strideIndex = index
        strideIndex *= stride
        if (posPos >= 0) {
            vertices[strideIndex + posPos + 0] = info.position.x
            vertices[strideIndex + posPos + 1] = info.position.y
            vertices[strideIndex + posPos + 2] = info.position.z
        }
        if (norPos >= 0) {
            vertices[strideIndex + norPos + 0] = info.normal.x
            vertices[strideIndex + norPos + 1] = info.normal.y
            vertices[strideIndex + norPos + 2] = info.normal.z
        }
        if (uvPos >= 0) {
            vertices[strideIndex + uvPos + 0] = info.uv.x
            vertices[strideIndex + uvPos + 1] = info.uv.y
        }
        if (colPos >= 0) {
            vertices[strideIndex + colPos + 0] = info.color.r
            vertices[strideIndex + colPos + 1] = info.color.g
            vertices[strideIndex + colPos + 2] = info.color.b
            vertices[strideIndex + colPos + 3] = info.color.a
        }
    }

    fun set(map: Pixmap) {
        if (map.width != width || map.height != height) throw GdxRuntimeException("Incorrect map size")
        set(map.pixels, map.format)
    }

    operator fun set(colorData: ByteBuffer, format: Pixmap.Format) {
        set(heightColorsToMap(colorData, format, width, height))
    }

    @JvmOverloads fun set(data: FloatArray, offset: Int = 0) {
        if (this.data.size > data.size - offset) throw GdxRuntimeException("Incorrect data size")
        System.arraycopy(data, offset, this.data, 0, this.data.size)
        update()
    }

    override fun dispose() {
        mesh.dispose()
    }

    companion object {

        fun heightColorsToMap(data: ByteBuffer, format: Pixmap.Format, width: Int, height: Int): FloatArray {
            val bytesPerColor = if (format == Format.RGB888) 3 else if (format == Format.RGBA8888) 4 else 0
            if (bytesPerColor == 0) throw GdxRuntimeException("Unsupported format, should be either RGB8 or RGBA8")
            if (data.remaining() < width * height * bytesPerColor) throw GdxRuntimeException("Incorrect map size")

            val startPos = data.position()
            var source: ByteArray? = null
            var sourceOffset = 0
            if (data.hasArray() && !data.isReadOnly) {
                source = data.array()
                sourceOffset = data.arrayOffset() + startPos
            } else {
                source = ByteArray(width * height * bytesPerColor)
                data.get(source)
                data.position(startPos)
            }

            val dest = FloatArray(width * height)
            for (i in dest.indices) {
                var v = source!![sourceOffset + i * bytesPerColor].toInt()
                v = if (v < 0) 256 + v else v
                dest[i] = v.toFloat() / 255f
            }

            return dest
        }
    }
}
