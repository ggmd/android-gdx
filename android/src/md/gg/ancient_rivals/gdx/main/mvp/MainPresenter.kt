package md.gg.ancient_rivals.gdx.main.mvp

class MainPresenter(model: MainModel, view: MainView) {
    init {
        view.setData(model.getMap())

        view.dragSubject.subscribe {
            view.pan(it)
        }

        view.scaleSubject.subscribe {
            view.scale(it.toDouble())
        }
    }
}