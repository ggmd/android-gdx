package md.gg.ancient_rivals.gdx.main.mvp

import md.gg.ancient_rivals.gdx.data.MapRepository
import md.gg.ancient_rivals.gdx.data.domain.GameMap
import md.gg.ancient_rivals.gdx.data.domain.prepare


class MainModel(private val mapRepository: MapRepository) {
    fun getMap(): GameMap {
        val map = mapRepository.loadFromRawRes()
        map.prepare()
        return map
    }
}