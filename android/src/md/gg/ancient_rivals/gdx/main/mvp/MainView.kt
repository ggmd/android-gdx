package md.gg.ancient_rivals.gdx.main.mvp

import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import io.reactivex.subjects.BehaviorSubject
import md.gg.ancient_rivals.gdx.data.domain.GameMap
import md.gg.ancient_rivals.gdx.main.MainActivity
import md.gg.ancient_rivals.gdx.main.MainGestureListener
import md.gg.ancient_rivals.gdx.main.MainSceneManager

class MainView(private val activity: MainActivity, private val gestureListener: MainGestureListener, private val mainSceneManager: MainSceneManager) {
    val dragSubject: BehaviorSubject<Pair<Float, Float>> = BehaviorSubject.create()
    val scaleSubject: BehaviorSubject<Float> = BehaviorSubject.create()

    val view: View

    init {
        view = setupView()

        gestureListener.apply {
            onPan.subscribe { dragSubject.onNext(it) }
            onZoom.subscribe { scaleSubject.onNext(it) }
        }
    }

    fun setData(map: GameMap) {
        mainSceneManager.onInit.subscribe {
            mainSceneManager.apply {
                buildMap(map)
                registerGestureListener(gestureListener)
            }
        }
    }

    fun pan(distance: Pair<Float, Float>) {
        mainSceneManager.pan(distance)
    }

    fun scale(factor: Double) {
        mainSceneManager.scale(factor)
    }

    private fun setupView(): View {
        val layout = RelativeLayout(activity)
        val gameView = activity.initializeForView(mainSceneManager)
        val textView = TextView(activity).apply { text = "Ancient Rivals " }
        layout.addView(gameView)
        val textViewParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)
        textViewParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        textViewParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
        layout.addView(textView, textViewParams)
        return layout
    }
}