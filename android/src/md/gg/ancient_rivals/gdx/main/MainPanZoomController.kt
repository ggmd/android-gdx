package md.gg.ancient_rivals.gdx.main

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import io.reactivex.subjects.BehaviorSubject

class MainPanZoomController(private val context: Context) {
    val onPan: BehaviorSubject<Pair<Float, Float>> = BehaviorSubject.create()
    val onZoom: BehaviorSubject<Float> = BehaviorSubject.create()

    private val panListener = object : GestureDetector.SimpleOnGestureListener() {
        override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
            onPan.onNext(Pair(distanceX, distanceY))
            return true
        }
    }

    private val scaleListener = object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        var lastSpan = 0F

        override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
            lastSpan = detector?.currentSpan ?: 0F
            return true
        }

        override fun onScale(detector: ScaleGestureDetector?): Boolean {
            val factor = lastSpan / detector?.currentSpan as Float
            onZoom.onNext(factor)
            return true
        }
    }

    fun init(view: View) {
        val panDetector = GestureDetector(context, panListener)
        val scaleDetector = ScaleGestureDetector(context, scaleListener)

        view.setOnTouchListener { v, event ->
            var ret = scaleDetector.onTouchEvent(event)
            ret = panDetector.onTouchEvent(event) or ret
            ret or v.onTouchEvent(event)
        }
    }
}