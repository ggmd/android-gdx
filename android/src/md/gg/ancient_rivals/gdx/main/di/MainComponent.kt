package md.gg.ancient_rivals.gdx.main.di

import dagger.Component
import md.gg.ancient_rivals.gdx.app.di.AppComponent
import md.gg.ancient_rivals.gdx.main.MainActivity

@MainScope
@Component(modules = arrayOf(MainModule::class), dependencies = arrayOf(AppComponent::class))
interface MainComponent {
    fun inject(activity: MainActivity)
}