package md.gg.ancient_rivals.gdx.main.di

import javax.inject.Scope

@Scope @Retention annotation class MainScope