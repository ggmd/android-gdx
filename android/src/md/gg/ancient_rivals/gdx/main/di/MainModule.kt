package md.gg.ancient_rivals.gdx.main.di

import dagger.Module
import dagger.Provides
import md.gg.ancient_rivals.gdx.data.MapRepository
import md.gg.ancient_rivals.gdx.main.MainActivity
import md.gg.ancient_rivals.gdx.main.MainGestureListener
import md.gg.ancient_rivals.gdx.main.MainSceneManager
import md.gg.ancient_rivals.gdx.main.Map3DGenerator
import md.gg.ancient_rivals.gdx.main.mvp.MainModel
import md.gg.ancient_rivals.gdx.main.mvp.MainPresenter
import md.gg.ancient_rivals.gdx.main.mvp.MainView

@Module
class MainModule(private val activity: MainActivity) {
    @MainScope
    @Provides
    fun model(mapRepository: MapRepository): MainModel {
        return MainModel(mapRepository)
    }

    @MainScope
    @Provides
    fun presenter(model: MainModel, view: MainView): MainPresenter {
        return MainPresenter(model, view)
    }

    @MainScope
    @Provides
    fun view(gestureListener: MainGestureListener, mainSceneManager: MainSceneManager): MainView {
        return MainView(activity, gestureListener, mainSceneManager)
    }

    @MainScope
    @Provides
    fun mainSceneManager(mapGenerator: Map3DGenerator): MainSceneManager {
        return MainSceneManager(activity, mapGenerator)
    }

    @MainScope
    @Provides
    fun mapGenerator(): Map3DGenerator {
        return Map3DGenerator(activity)
    }

    @MainScope
    @Provides
    fun mainGestureListener(): MainGestureListener {
        return MainGestureListener()
    }
}