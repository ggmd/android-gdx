package md.gg.ancient_rivals.gdx.main

import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import md.gg.ancient_rivals.gdx.app.AncientRivalsApp
import md.gg.ancient_rivals.gdx.main.di.DaggerMainComponent
import md.gg.ancient_rivals.gdx.main.di.MainModule
import md.gg.ancient_rivals.gdx.main.mvp.MainPresenter
import md.gg.ancient_rivals.gdx.main.mvp.MainView
import javax.inject.Inject

class MainActivity : AndroidApplication() {
    @Inject
    lateinit var presenter: MainPresenter
    @Inject
    lateinit var view: MainView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setup()

        setContentView(view.view)
    }

    private fun setup() {
        DaggerMainComponent.builder()
                .appComponent((application as AncientRivalsApp).appComponent)
                .mainModule(MainModule(this))
                .build().inject(this)
    }
}
