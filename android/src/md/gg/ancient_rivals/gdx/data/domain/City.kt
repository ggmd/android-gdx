package md.gg.ancient_rivals.gdx.data.domain

import java.io.Serializable

data class City(var name: String = "") : Serializable