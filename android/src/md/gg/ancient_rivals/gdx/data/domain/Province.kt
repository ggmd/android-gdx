package md.gg.ancient_rivals.gdx.data.domain

import java.io.Serializable

data class Province(
        var name: String = "",
        var capital: Civilization = Civilization.NONE,
        var type: ProvinceType = ProvinceType.TERRAIN,
        var cities: MutableList<City> = mutableListOf(),
        var resources: MutableList<Resource> = mutableListOf(),
        var mapConnections: List<Pair<MapNode, MapNode>> = listOf()
) : Serializable

fun Province.placeName(): Pair<Double, Double> {
    var tx = 0.0
    var ty = 0.0
    mapConnections.forEach {
        tx += it.first.x
        ty += it.second.y
    }
    return Pair(tx / mapConnections.size, ty / mapConnections.size)
}

fun Province.nodes(): List<MapNode> {
    val result: MutableList<MapNode> = mutableListOf()
    for ((f, _) in mapConnections) {
        result.add(f)
    }
    return result
}

fun Province.otherTypes(provinces: List<Province>): List<Province> = provinces.filter { it.type != type }

fun connectionsAreEqual(connection: Pair<MapNode, MapNode>, connection1: Pair<MapNode, MapNode>): Boolean {
    val result = (connection.first.valueCompare(connection1.first) and connection.second.valueCompare(connection1.second)) or
            (connection.first.valueCompare(connection1.second) and connection.second.valueCompare(connection1.first))
    return result
}
