package md.gg.ancient_rivals.gdx.data.domain

import java.io.Serializable

data class MapNode(var x: Double = 0.0, var y: Double = 0.0) : Serializable

fun MapNode.valueCompare(node: MapNode): Boolean {
    return (Math.abs(Math.abs(x) - Math.abs(node.x)) < 1.0) and (Math.abs(Math.abs(y) - Math.abs(node.y)) < 1.0)
}
