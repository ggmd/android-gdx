package md.gg.ancient_rivals.gdx.data.domain

import java.io.Serializable

enum class ProvinceType(var type: String) : Serializable {
    TERRAIN("Terrain"), SEA("Sea")
}