package md.gg.ancient_rivals.gdx.data

import android.content.Context
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import md.gg.ancient_rivals.gdx.R
import md.gg.ancient_rivals.gdx.data.domain.GameMap


class MapRepository(
        private val context: Context,
        private val gson: Gson
) {
    fun loadFromRawRes(): GameMap {
        val resource = context.resources.openRawResource(R.raw.map)
        val json = resource.bufferedReader().use { it.readText() }
        return gson.fromJson(json)
    }
}
